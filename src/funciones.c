#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cabecera.h"
#define TAM 20

cola *crear_cola(){
	cola *colaNueva = malloc(sizeof(cola));
	if(colaNueva==NULL){
		return NULL;
	}else{
		colaNueva->tamano = 0;
		colaNueva->inicio = NULL;
		colaNueva->fin = NULL;
		return colaNueva;
	}
}

int encolar(cola *mi_cola, void *elemento){
	if(elemento==NULL){
		return -1;
	}

	nodo_cola *node = malloc(sizeof(nodo_cola));
	if(node==NULL){
		return -1;
	}

	node->elemento = elemento;
	if(mi_cola->tamano == 0){
		mi_cola->inicio = node;
		mi_cola->fin = node;
		node->anterior = NULL;
		node->siguiente = NULL;
		mi_cola->tamano++;
		return 0;

	}else{
		mi_cola->fin->siguiente = node;
		node->anterior = mi_cola->fin;
		mi_cola->fin = node;
		mi_cola->fin->siguiente = NULL;
		mi_cola->tamano++;
		return 0;
	}
}

void *decolar(cola *mi_cola){
	if(mi_cola->tamano==0){
		return NULL;
	}

	void *element = mi_cola->inicio->elemento;
	if(mi_cola->tamano == 1){
		free(mi_cola->inicio);
		mi_cola->inicio = NULL;
		mi_cola->fin = NULL;
		mi_cola->tamano = 0;
		return element;
		
	}else{
		mi_cola->inicio = mi_cola->inicio->siguiente;
		mi_cola->inicio->anterior->siguiente=NULL;
		free(mi_cola->inicio->anterior);
		mi_cola->inicio->anterior = NULL;
		mi_cola->tamano--;
		return element;
	}

}

unsigned long tamano_cola(cola *mi_cola){
	return mi_cola->tamano;
}

unsigned long position_cola(cola *mi_cola, void *elemento){
	if(elemento==NULL){
		return -1;
	}
	nodo_cola *nodo=mi_cola->inicio;
	long size = mi_cola->tamano;
	for(int i=0;i<size;i++){
		if(*(long*)elemento==*(long *)(nodo->elemento)){
			return i;
		}
		nodo=nodo->siguiente;
	}
	return -1;
}	

int destruir_cola(cola *mi_cola){
	if(mi_cola->tamano != 0){
		return -1;
	}
	free(mi_cola);
	return 0;
}





