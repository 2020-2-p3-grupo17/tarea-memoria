#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cabecera.h"
#define TAM 20

int main(){
	cola *mi_cola = crear_cola();
	char estado = 1;
	long *p;
	char entrada[TAM];
	while (estado){
		memset(entrada,0,TAM);
		fgets(entrada,TAM,stdin);
		if(strcmp(entrada,"x")==0 || strcmp(entrada, "x\n")==0){
			estado = 0;
		}else{
			p=malloc(sizeof(long));
			*p=atoi(entrada);
			encolar(mi_cola,p);
		}
	}
	printf("Encolando elementos...\n");
	printf("La cola tiene %ld elementos. \n", tamano_cola(mi_cola));
	printf("Elemento a buscar: ");
	memset(entrada,0,TAM);
	fgets(entrada,TAM,stdin);
	p=malloc(sizeof(long));
	*p=atoi(entrada);
	printf("El elemento esta en la posicion %ld de la cola. \n", position_cola(mi_cola, p));
	free(p);
	while (tamano_cola(mi_cola) > 0){
		void *e = decolar(mi_cola);
		free(e);
	}
	printf("Elementos decolados. \n");
	destruir_cola(mi_cola);
	printf("Saliendo...\n");
	return 0;
}
