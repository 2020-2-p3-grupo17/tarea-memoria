CC=gcc -Wall -c
IC=-I include/
FS=-fsanitize=address,undefined
bin/ejecutable: obj/funciones.o obj/main.o
	mkdir -p bin/
	gcc $^ -o $@ $(FS)
obj/funciones.o: src/funciones.c
	mkdir -p obj/ 
	$(CC) $(IC) $^ -o $@ $(FS)
obj/main.o: src/main.c
	$(CC) $(IC) $^ -o $@ $(FS)
.PHONY: clean
clean:
	rm obj/* bin/*
